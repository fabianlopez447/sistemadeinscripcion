<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>['admin']], function (){
	Route::get('/consultar', function () {
    return view('consultar');
	});
	Route::get('/insertar', function () {
	    return view('insertar');
	});
	Route::get('/reportes', function () {
	    return view('reportes');
	});
	Route::resource('alumnos', 'alumnosController');
	Route::resource('aula', 'aulaController');
	Route::resource('profesores', 'profesoresController');
	Route::resource('inscripcion', 'inscripcionesController');
	Route::post('nota/store',[
		'uses' => 'notasController@store',
		 'as'  => 'nota.store'
	]);
	Route::get('nota/{id}/consultar',[
		'uses' => 'notasController@consultar',
		 'as'  => 'nota.consultar'
	]);
	Route::get('notas/index',[
		'uses' => 'notasController@index',
		 'as'  => 'notas'
	]);
	Route::resource('notas', 'notasController');
	Route::get('cuentas/create',[
		'uses' => 'HomeController@create',
		 'as'  => 'cuentas.create'
	]);
	Route::post('cuentas/store',[
		'uses' => 'HomeController@store',
		 'as'  => 'cuentas.store'
	]);
	Route::resource('representantes', 'representantesController');
	Route::get('representante/create/{cedula}', [
		'uses'	=> 'representantesController@create',
		'as'	=> 'representante.create'
	]);
	  Route::post('profesores/cedula', [
		'uses'	=> 'profesoresController@cedula',
		'as'	=> 'profesores.cedula'
	]);
	Route::get('alumno/validar/{cedula}', [
		'uses'	=> 'alumnosController@validar',
		'as'	=> 'alumno.validar'
	]);
  Route::get('alumno/getalumno/{id}', [
    'uses'	=> 'alumnosController@getAlumno',
    'as'	=> 'alumno.getAlumno'
  ]);

    Route::get('alumnospdf', function(){
    	$items = App\Alumno::all();
    	$pdf = PDF::loadView('reportepdf.alumnopdf', ['items' => $items]);
    	return $pdf->stream('alumnos.pdf');
    });
    Route::get('profesorespdf', function(){
    	$items = App\Profesor::all();
    	$pdf = PDF::loadView('reportepdf.profesorpdf', ['items' => $items]);
    	return $pdf->stream('profesores.pdf');
    });
    Route::get('representantespdf', function(){
    	$items = App\Representante::all();
    	$pdf = PDF::loadView('reportepdf.representantepdf', ['items' => $items]);
    	return $pdf->stream('representantes.pdf');
    });
    Route::get('inscripcionespdf', function(){
    	$items = App\Alumno::all();
    	$pdf = PDF::loadView('reportepdf.inscripcionpdf', ['items' => $items]);
    	return $pdf->stream('inscripciones.pdf');
    });

    Route::get('repalumnospdf/{id}/',[
		'uses' => 'alumnosController@reporte',
		 'as'  => 'reportealumnos'
	]);

    Route::post('validarcedula',[
		'uses' => 'representantesController@validar',
		 'as'  => 'validarcedula'
	]);

});
