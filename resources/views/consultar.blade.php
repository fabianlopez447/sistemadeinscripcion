@extends('layouts.app')

@section('content')  
    <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Consultar - Modificar</a></div>
  </div>
<div  class="quick-actions_homepage">
    <ul class="quick-actions">
      <li class="bg_lo"> <a href="{{url('inscripcion')}}"> <i class="icon-group"></i> Inscripción</a> </li>
      <li class="bg_lo"> <a href="{{url('representantes')}}"> <i class="icon-user"></i> Representante</a> </li>
      <li class="bg_lo"> <a href="{{url('alumnos')}}"> <i class="icon-user"></i> Alumno</a> </li>
      <li class="bg_lo"> <a href="{{url('profesores')}}"> <i class="icon-book"></i> Profesor</a> </li>
      <li class="bg_lo"> <a href="{{url('notas/index')}}"> <i class="icon-folder-open"></i> Notas</a> </li>
    </ul>
  </div>
@endsection
