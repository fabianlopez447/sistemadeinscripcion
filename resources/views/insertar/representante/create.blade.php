@extends('layouts.app')

@section('content')
    <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Insertar </a> <a href="#" class="tip-bottom">Representante</a> <a href="#" class="current"></div>
  </div>


<div class="span10">
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Información del Representante con C.I: {{$cedula}}</h5>
        </div>
        <div class="widget-content nopadding">
      
          <form action="{{route('representantes.store')}}" class="form-horizontal" method="post">
            {{ csrf_field() }}
            <input type="hidden" class="span7" placeholder="Cedula" value="{{$cedula}}"  name="cedula" required/>
            <div class="control-group">
              <label class="control-label">Nombres:</label>
              <div class="controls">
                <input type="text" class="span7" placeholder="Nombres"  name="nombre" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Apellidos:</label>
              <div class="controls">
                <input type="text" class="span7" placeholder="Apellidos"  name="apellidos" required/>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Teléfono:</label>
              <div class="controls">
                <input type="number" class="span7" maxlength="11" placeholder="Teléfono"  name="telefono" required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Correo Electrónico</label>
              <div class="controls">
                <input type="email" class="span7" placeholder="Correo"  name="correo" required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Dirección:</label>
              <div class="controls">
                <input type="text" class="span7" placeholder="Dirección" name="direccion" required/>
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Guardar datos del representante</button>
            </div>
          </form>
          </div>
        </div>
</div>


@endsection
