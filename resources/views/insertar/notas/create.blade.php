@extends('layouts.app')

@section('content')
      <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Insertar </a> <a href="#" class="tip-bottom">Notas</a> <a href="#" class="current"></div>
  </div>


<div class="span10">
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Información de notas del Alumno</h5>
        </div>
        <div class="widget-content nopadding">
      
           <form action="{{route('nota.store')}}" class="form-horizontal" method="post">
            {{ csrf_field() }}

           <div class="control-group">
              <label class="control-label">Seleccione alumno Alumno</label>
              <div class="controls">
                <select name="id_alumno"  >
                     @foreach($alumns as $alumns)
                             <option value="{{$alumns->id}}">{{$alumns->nombre}} {{$alumns->apellidos}}</option>
                    @endforeach   
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Profesor</label>
              <div class="controls">
                <select name="id_profesor"  >
                     @foreach($profs as $prof)
                             <option value="{{$prof->id}}">{{$prof->cedula}} {{$prof->nombre}} {{$prof->apellidos}}</option>
                    @endforeach   
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Materia:</label>
              <div class="controls">
                <input type="text" class="span7" placeholder="Materia" name="materia" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Calificaión:</label>
              <div class="controls">
                <input type="text" class="span7" placeholder="Calificación" name="calificacion" required />
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Guarda notas del estudiante</button>
            </div>
          </form>
          </div>
        </div>
</div>



@endsection
