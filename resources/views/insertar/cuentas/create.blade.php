@extends('layouts.app')

@section('content')
    <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Insertar </a> <a href="#" class="tip-bottom">Cuenta</a> <a href="#" class="current"></div>
  </div>


<div class="span10">
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Nueva Cuenta </h5>
        </div>
        <div class="widget-content nopadding">
      
          <form class="form-horizontal" method="POST" action="{{ route('cuentas.store') }}">
            {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Nombre:</label>
              <div class="controls">
                <input id="name" type="text" class="span7" name="name" placeholder="Nombres" value="{{ old('name') }}" required autofocus>
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                      @endif
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Correo:</label>
              <div class="controls">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="span7"  name="email" value="{{ old('email') }}" required>
                     @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                </div>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Contraseña:</label>
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <div class="controls">
                    <input id="password" type="password" class="span7" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                  </div>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label"> Confirmar Contraseña:</label>
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <div class="controls">
                    <input id="password-confirm" type="password" class="span7" name="password_confirmation" required>
                  </div>
              </div>
            </div>
            <div class="control-group">
                    <label class="control-label">Tipo de cuenta</label>
                     <div class="controls">
                       <select name="nivel" id="">
                        <option value="1">Operador</option>
                        <option value="0">Administrador</option>
                       </select>
                    </div>
              </div>         
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Guardar nueva cuenta</button>
            </div>
          </form>
    </div>
</div>

@endsection
