@extends('layouts.app')

@section('content')
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Insertar </a> <a href="#" class="tip-bottom">Alumno - Inscripción</a> <a href="#" class="current"></div>

<div class="span10" id="app">
  <br>
  <div v-if="nuevo" class="alert alert-info alert-block">
              <h4 class="alert-heading">Alerta</h4>
              La cedula del representante no se encuentra registratada en el sistema </div>
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Inscripción - Información del Alumno</h5>
        </div>
        <div class="widget-content nopadding">
          <form method="post" class="form-horizontal" action="" v-on:submit.prevent="validar">
            <div class="control-group">
              <label class="control-label">Cedula del representante:</label>
              <div class="controls">
                <input type="number" class="span7" v-model="cedula" placeholder="Nombres" name="Ingrese cedula del representante" min="999999" max="99999999" />
              </div>
            </div>
          </form >
          <form v-show="mostrarcampos" action="{{route('alumnos.store')}}" method="post" class="form-horizontal">
            {{ csrf_field() }}
           <div v-show="mostrarcampos">
                <div class="control-group">

             <label class="control-label">Nombre del representate:</label><div class="control-group">
                    <div class="controls"> <input type="text" class="span7" V-model = "representante.nombre +' '+ representante.apellidos" disabled="true" />
                     </div> </div>
              <label class="control-label">Direccion:</label><div class="control-group">
                 <div class="controls"><input type="text" class="span7" V-model = "representante.direccion" disabled="true" />
                  </div></div>

                   <div class="control-group">
                    <label class="control-label">Fecha de inscripcion:</label>
                    <div class="controls">
                      <input type="text" class="span7" disabled="" value="{{date('d/m/Y')}}" required/>
                      <input type="hidden" name="fecha_ing" class="span7" disabled="" value="{{date('d/m/Y')}}" required/>
                    </div>
                </div>
              <label class="control-label">Identificador:</label>
                    <div class="controls">
                      <input type="text" class="span7" v-bind:value="representante.cedula+'-'+parseInt(cantidad+1)" name="nombres" placeholder="indentificador" required/>  
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Fecha de Nacimiento:</label>
                    <div class="controls">
                      <input id="datepick" max="0" min="0" type="date" name="fecha_nac" class="span7" required/>
                    </div>
                </div>
                <div class="control-group">
              <label class="control-label">Nombres:</label>
                    <div class="controls">
                      <input type="text" class="span7" name="nombres" placeholder="Nombres" required/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Apellidos:</label>
                    <div class="controls">
                      <input type="text" class="span7" name="apellidos" placeholder="Apellidos" required />
                    </div>
                </div>


                <div class="control-group">
                    <label class="control-label">Partida de Nacimiento</label>
                    <div class="controls">
                      <label>
                        <input type="radio" value="Si" name="partida_nac"  />
                        Si</label>
                         <label>
                        <input type="radio" value="No" name="partida_nac" required />
                        No</label>
                      </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Copia de cedula Representante</label>
                    <div class="controls">
                      <label>
                                <input type="radio" value="Si" name="cedula_rep"  />
                                Si</label>
                              <label>
                                <input type="radio" value="No" name="cedula_rep" required  />
                                No
                             </label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Representante</label>
                    <div class="controls">
                      <select name="id_representante"  >
                           @foreach($repres as $repre)
                                   <option value="{{$repre->id}}">{{$repre->cedula}} - {{$repre->nombre}}</option>
                          @endforeach
                      </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Profesor</label>
                    <div class="controls">
                      <select name="id_profesor">
                           @foreach($prof as $profs)
                                   <option value="{{$profs->id}}">{{$profs->cedula}} - {{$profs->nombre}}</option>
                          @endforeach
                      </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Sección</label>
                    <div class="controls">
                      <select name="seccion" >
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                      </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Aula</label>
                    <div class="controls">
                       <select name="aula" >
                        @for($i=1; $i < 11; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                      </select>
                    </div>
               </div>
              <div class="control-group">
                    <label class="control-label">Turno</label>
                    <div class="controls">
                      <select name="turno" >
                        <option value="Mañana">Mañana</option>
                        <option value="Tarde">Tarde</option>
                      </select>
                    </div>
              </div>
              <div class="control-group">
                  <label class="control-label">Descripción</label>
                  <div class="controls">
                    <textarea name="descripcion" class="span7" ></textarea>
                  </div>
              </div>
              <div class="form-actions">
                  <button type="submit" class="btn btn-success">Guardar Registros de Inscripcion</button>
              </div>
          </div>
          </form>
          </div>
        </div>
        </div>
</div>

@endsection
@section('js')

<script src="{!!asset('vue/inscripcion.js')!!}"></script>
<script type="text/javascript">
function soloNumeros( evt )
{
    if ( window.event ) { // IE
        keyNum = evt.keyCode;
    } else {
        keyNum = evt.which;
    }

    if ( keyNum >= 48 && keyNum <= 57 ) {
        return true;
    } else {
        return false;
    }
}
</script>
<script>
var dt = new Date();
// Display the month, day, and year. getMonth() returns a 0-based number.
var month = dt.getMonth()+1;
var day = dt.getDate();
var year = dt.getFullYear();
var anio = year + '-' + month + '-' + day;
document.getElementById("datepick").max= year-2+'-12-31';
document.getElementById("datepick").min= year-11+'-12-31';

</script>

@endsection
