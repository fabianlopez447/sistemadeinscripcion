@extends('layouts.app')

@section('content')
     <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Insertar </a> <a href="#" class="tip-bottom">Profesor</a> <a href="#" class="current"></div>
  </div>


<div class="span10">
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Información del profesor(a)</h5>
        </div>
        <div class="widget-content nopadding">
      
          <form class="form-horizontal" action="{{route('profesores.store')}}" method="post">
            {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Cedula:</label>
              <div class="controls">
                <input type="text" class="span7" maxlength="8" placeholder="Cedula" name="cedula" onKeyPress="return soloNumeros( event )" required />
              </div>
            </div>
            <div class="control-group" v-show="mostrar">
              <label class="control-label">Nombres:</label>
              <div class="controls">
                <input type="text" class="span7" placeholder="Nombres" name="nombre" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Apellidos:</label>
              <div class="controls">
                <input type="text" class="span7" placeholder="Apellidos" name="apellidos" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Dirección:</label>
              <div class="controls">
                <input type="text" class="span7" placeholder="Dirección" name="direccion"  required/>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Teléfono:</label>
              <div class="controls">
                <input type="number" class="span7" placeholder="Teléfono" name="telefono" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Correo Electronico:</label>
              <div class="controls">
                <input type="mail" class="span7" placeholder="Correo Electronico" name="correo"  required/>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Aula</label>
              <div class="controls">
                <select name="aula" >
                  @for($i=1; $i < 11; $i++)
                      <option value="{{ $i }}">{{ $i }}</option>    
                  @endfor
                </select>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Turno</label>
              <div class="controls">
                <select name="turno" >
                  <option value="Mañana">Mañana</option>
                  <option value="Tarde">Tarde</option>
                </select>
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Guardar datos del profesor</button>
            </div>
          </form>
          </div>
        </div>
</div>

<script type="text/javascript">
function soloNumeros( evt )
{
    if ( window.event ) { // IE
        keyNum = evt.keyCode;
    } else {
        keyNum = evt.which;
    }

    if ( keyNum >= 48 && keyNum <= 57 ) {
        return true;
    } else {
        return false;
    }
}
</script>

@endsection
