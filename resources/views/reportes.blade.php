@extends('layouts.app')

@section('content')
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Reportes</a></div>
      </div>
    <div  class="quick-actions_homepage">
        <ul class="quick-actions">
          <li class="bg_lo"> <a href="{{url('alumnospdf')}}"> <i class="icon-user"></i> Alumno</a> </li>
          <li class="bg_lo"> <a href="{{url('inscripcionespdf')}}"> <i class="icon-group"></i> Inscripción</a> </li>
          <li class="bg_lo"> <a href="{{url('representantespdf')}}"> <i class="icon-user"></i> Representante</a> </li>
          <li class="bg_lo"> <a href="{{url('profesorespdf')}}"> <i class="icon-book"></i> Profesor</a> </li>
        </ul>
      </div>
@endsection
