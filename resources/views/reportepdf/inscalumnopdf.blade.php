
<!DOCTYPE html>
<style>
  .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 18cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 5px 0;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 90px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(dimension.png);
}

#project {

}

#project span {
  color: #5D6975;

  width: 52px;
  margin-right: 10px;
  display: inline-block;
  font-size: 0.8em;
}

#company {
  float: right;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  padding: 10px;

}
td {
  padding: 30px;
}

table th {
  padding:  ;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 10px;
 
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
</style>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Reporte</title>
  </head>
  <body>
    <header class="clearfix">
      <h2>Reporte Inscripción - {{$item->nombres}} {{$item->apellidos}}  </h2>
    </header>
    <main>
          <table class="table table-bordered table-striped">
              <thead>
               
                 <!--  <th>ID</th>
                 <th>Nombre A.</th>
                 <th>Apellidos A.</th>
                 <th>Fecha Ing.</th>
                 <th>Representante</th>
                 <th>Profesor</th>
                 <th>Aula</th>
                 <th>Sección</th>
                 <th>Turno</th> -->
                  <tr>
                 <td><center><b>Nombre del alumno:</b></center></td>
                  <td>{{$item->nombres}}</td>
                  </tr>
                  <tr>
                 <td><center><b>Apellido del alumno:</b></center></td>
                  <td>{{$item->apellidos}}</td>
                  </tr>
                  <tr>
                  <td><center><b>Fecha de nacimiento:</b></center></td>
                  <td>{{$item->fecha_nac}}</td>
                  </tr>
                  <tr>
                  <td><center><b>Fecha de inscripción:</b></center></td>
                  <td>{{$item->fecha_ing}}</td>
                  </tr>
                  <tr>
                  <td><center><b>Representante:</b></center></td>
                  <td>{{$item->representante->cedula}} {{$item->representante->nombre}} {{$item->representante->apellidos}}</td>
                  </tr>
                  <tr>
                   <td><center><b>Copia Cedula Representante:</b></center></td>
                  <td>{{$item->cedula_rep}} </td>
                  </tr>
                   <tr>
                   <td><center><b>Copia Partida de Nacimiento:</b></center></td>
                  <td>{{$item->partida_nac}} </td>
                  </tr>
                  <tr>
                   <td><center><b>Profesor:</b></center></td>
                  <td>{{$item->profesor->cedula}} {{$item->profesor->nombre}} {{$item->profesor->apellidos}}</td>
                  </tr>
                  <tr>
                   <td><center><b>Aula:</b></center></td>
                  <td>{{$item->aula}} </td>
                  </tr>
                  <tr>
                   <td><center><b>Seccion:</b></center></td>
                  <td>{{$item->seccion}} </td>
                  </tr>
                  <tr>
                   <td><center><b>Turno:</b></center></td>
                  <td>{{$item->turno}} </td>
                  </tr>
            </table>
      <div id="notices">
        <div>Tipo de documento:</div>
        <div class="notice">Reporte de Inscripción de alumno</div>
      </div>
    </main>
    <footer>
      U.E Colegio Simoncito Las Americas.
    </footer>
  </body>
</html>
   
  </center>