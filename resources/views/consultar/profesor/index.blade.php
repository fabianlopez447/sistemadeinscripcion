@extends('layouts.app')

@section('content')
    <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Consultar</a> <a href="#" class="tip-bottom">Profesor</a> <a href="#" class="current"></div>
  </div>


<div class="span10">

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Consultar Profesores</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre </th>
                  <th>Apellidos </th>
                  <th>Cedula</th>
                  <th>Telefono</th>
                  <th>Correo</th>
                  <th>Dirección</th>
                   <th>Aula</th>
                  <th>Turno</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                 @foreach($items as $item)
                  <tr class="odd gradeX">
                    <td>{{$item->id}}</td>
                    <td>{{$item->nombre}}</td>
                    <td>{{$item->apellidos}}</td>
                    <td>{{$item->cedula}}</td>
                    <td>{{$item->telefono}}</td>
                    <td>{{$item->correo}}</td>
                    <td>{{$item->direccion}}</td>
                    <td>{{$item->aula}}</td>
                    <td>{{$item->turno}}</td>
                    <td >  <center> <a href="{{route('profesores.edit', $item->id)}}" class="btn btn-mini btn-warning">Modificar</a></center></td>
                  </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
        </div>
        
</div>
@endsection
