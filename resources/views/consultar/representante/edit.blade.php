@extends('layouts.app')

@section('content')
   <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Insertar </a> <a href="#" class="tip-bottom">Representante</a> <a href="#" class="current"></div>
  </div>


<div class="span10">
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Información del Representante</h5>
        </div>
        <div class="widget-content nopadding">
      
          {!!Form::model($item, ['route'=>['representantes.update', $item->id], 'method'=>'PUT', 'class'=>'form-horizontal'])!!}
            <div class="control-group">
              <label class="control-label">Nombres:</label>
              <div class="controls">
                {!!Form::text('nombre', null,['class'=>'span7'])!!}
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Apellidos:</label>
              <div class="controls">
                {!!Form::text('apellidos', null,['class'=>'span7'])!!}
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Cedula:</label>
              <div class="controls">
                {!!Form::text('cedula', null,['class'=>'span7'])!!}
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Dirección:</label>
              <div class="controls">
                {!!Form::text('direccion', null,['class'=>'span7'])!!}
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Teléfono:</label>
              <div class="controls">
                {!!Form::text('telefono', null,['class'=>'span7'])!!}
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Correo</label>
              <div class="controls">
                {!!Form::text('correo', null,['class'=>'span7'])!!}
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Aceptar</button>
            </div>
          {!!Form::close()!!}
          </div>
        </div>
</div>



<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2018 &copy; Unidad Educativa Simoncito "Las Americas" <a href="http://themedesigner.in"> - Desarrollado por: Francisco La Rosa</a> </div>
</div>

@endsection
