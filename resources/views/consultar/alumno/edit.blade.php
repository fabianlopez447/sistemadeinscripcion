@extends('layouts.app')

@section('content')
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Insertar </a> <a href="#" class="tip-bottom">Alumno - Editar</a> <a href="#" class="current"></div>
  
<div class="span10">
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Editar - Información del Alumno</h5>
        </div>
        <div class="widget-content nopadding">
      
          {!!Form::model($item,['route'=>['alumnos.update', $item->id],'method'=>'PUT','class' => 'form-horizontal', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
            {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Nombres:</label>
              <div class="controls">
                <input type="text" class="span7" value="{{$item->nombres}}" c name="nombres" placeholder="Nombres" required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Apellidos:</label>
              <div class="controls">
                <input type="text" class="span7" name="apellidos" value="{{$item->apellidos}}" placeholder="Apellidos" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Fecha de Nacimiento:</label>
              <div class="controls">
                <input type="date" name="fecha_nac" class="span7" value="{{$item->fecha_nac}}" required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Fecha de Ingreso:</label>
              <div class="controls">
                <input type="date" name="fecha_ing" value="{{$item->fecha_ing}}"  class="span7" required/>
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Aceptar</button>
            </div>
          {!!Form::close()!!}
          </div>
        </div>
</div>
@endsection
