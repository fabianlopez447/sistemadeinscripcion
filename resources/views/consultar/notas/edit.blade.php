@extends('layouts.app')

@section('content')
    <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Insertar </a> <a href="#" class="tip-bottom">Notas</a> <a href="#" class="current"></div>
  </div>


<div class="span10">
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Información de notas del Alumno</h5>
        </div>
        <div class="widget-content nopadding">
      
          {!!Form::model($item, ['route'=>['notas.update', $item->id], 'method'=>'PUT', 'class'=>'form-horizontal'])!!}
            <div class="control-group">
              <label class="control-label">Materia:</label>
              <div class="controls">
                {!!Form::text('materia', null,['class'=>'span7'])!!}
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Calificación:</label>
              <div class="controls">
                {!!Form::number('calificacion', null,['class'=>'span7'])!!}
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Aceptar</button>
            </div>
          {!!Form::close()!!}
          </div>
        </div>
</div>



<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2018 &copy; Unidad Educativa Simoncito "Las Americas" <a href="http://themedesigner.in"> - Desarrollado por: Francisco La Rosa</a> </div>
</div>

@endsection
