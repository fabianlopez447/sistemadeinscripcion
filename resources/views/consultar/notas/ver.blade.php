@extends('layouts.app')

@section('content')
    <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Consultar</a> <a href="#" class="tip-bottom">Notas</a> <a href="#" class="current"></div>
  </div>


<div class="span10">

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Ver Notas</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre Materia</th>
                  <th>Calificación</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                 @foreach($items as $item)
                  <tr class="odd gradeX">
                    <td>{{$item->id}}</td>
                    <td>{{$item->materia}}</td>
                    <td>{{$item->calificacion}}</td>
                    <td >  <center> <a href="{{route('notas.edit', $item->id)}}" class="btn btn-mini btn-warning">Modificar</a></center></td>
                  </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
        </div>
        
</div>

@endsection