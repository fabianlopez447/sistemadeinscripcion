@extends('layouts.app')

@section('content')
   <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Consultar</a> <a href="#" class="tip-bottom">Inscripción</a> <a href="#" class="current"></div>
  </div>


<div class="span10">

        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Consultar Representantes</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre Alumno</th>
                  <th>Apellidos Alumno</th>
                  <th>Fecha Ingreso</th>
                  <th>Representante</th>
                  <th>Profesor</th>
                  <th>Aula</th>
                  <th>Sección</th>
                  <th>Turno</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                 @foreach($items as $item)
                  <tr class="odd gradeX">
                    <td>{{$item->id}}</td>
                    <td>{{$item->nombres}}</td>
                    <td>{{$item->apellidos}}</td>
                    <td>{{$item->fecha_ing}}</td>
                    <td>{{$item->representante->cedula}} {{$item->representante->nombre}} {{$item->representante->apellidos}}</td>
                    <td>{{$item->profesor->cedula}} {{$item->profesor->nombre}} {{$item->profesor->apellidos}}</td>
                    <td>{{$item->aula}}</td>
                    <td>{{$item->seccion}}</td>
                    <td>{{$item->turno}}</td>
                    <td >  <center> <a href="{{route('inscripcion.edit', $item->id)}}" class="btn btn-mini btn-warning">Modificar</a> <a href="{{url('repalumnospdf', $item->id)}}" class="btn btn-mini btn-primary">PDF</a></center></td>
                  </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
        </div>
        
</div>



<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2018 &copy; Unidad Educativa Simoncito "Las Americas" <a href="http://themedesigner.in"> - Desarrollado por: Francisco La Rosa</a> </div>
</div>
@endsection
