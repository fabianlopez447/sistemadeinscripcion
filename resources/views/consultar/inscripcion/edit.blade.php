@extends('layouts.app')

@section('content')
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>Consultar </a> <a href="#" class="tip-bottom">Inscripción - Editar</a> <a href="#" class="current"></div>
  
<div class="span10">
 <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Editar - Información de inscripción</h5>
        </div>
        <div class="widget-content nopadding">
      
          {!!Form::model($item,['route'=>['inscripcion.update', $item->id],'method'=>'PUT','class' => 'form-horizontal', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
            {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Nombres:</label>
              <div class="controls">
                <input type="text" class="span7" value="{{$item->nombres}}" c name="nombres" placeholder="Nombres" required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Apellidos:</label>
              <div class="controls">
                <input type="text" class="span7" name="apellidos" value="{{$item->apellidos}}" placeholder="Apellidos" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Fecha de Ingreso:</label>
              <div class="controls">
                <input type="date" name="fecha_ing" value="{{$item->fecha_ing}}"  class="span7" required/>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Partida de Nacimiento</label>
              <div class="controls">
                <label>
                   {!!Form::radio('partida_nac', 'Si' , true)!!}
                  Si</label>
                   <label>
                  {!!Form::radio('partida_nac', 'No' , true)!!}
                  No</label>
                </div>
              </div>
              <div class="control-group">
              <label class="control-label">Copia de cedula Representante</label>
              <div class="controls">
                <label>
                          {!!Form::radio('cedula_rep', 'Si' , true)!!}
                          Si</label>
                        <label>
                         {!!Form::radio('cedula_rep', 'No' , true)!!}
                          No
                       </label>
              </div>
            <div class="control-group">
              <label class="control-label">Representante</label>
              <div class="controls">
                <select name="id_representante"  >
                  <option selected value="{{$item->id_representante}}">{{$item->representante->cedula}} - {{$item->representante->nombre}}</option>
                     @foreach($repres as $repre)

                             <option value="{{$repre->id}}">{{$repre->cedula}} - {{$repre->nombre}}</option>
                    @endforeach   
                </select>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Profesor</label>
              <div class="controls">
                <select name="id_profesor">
                  <option selected value="{{$item->id_profesor}}">{{$item->profesor->cedula}} - {{$item->profesor->nombre}}</option>
                     @foreach($prof as $profs)
                             <option value="{{$profs->id}}">{{$profs->cedula}} - {{$profs->nombre}}</option>
                    @endforeach   
                </select>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Sección</label>
              <div class="controls">
                <select name="seccion" >
                  <option selected value="{{$item->seccion}}">{{$item->seccion}}</option>
                  <option value="A">A</option>
                  <option value="B">B</option>
                  <option value="C">C</option>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Aula</label>
              <div class="controls">
                 <select name="aula" >
                  <option selected value="{{$item->aula}}">{{$item->aula}}</option>
                  @for($i=1; $i < 11; $i++)
                      <option value="{{ $i }}">{{ $i }}</option>    
                  @endfor
                </select>

            </div>
            <div class="control-group">
              <label class="control-label">Turno</label>
              <div class="controls">
                <select name="turno" >
                  <option selected value="{{$item->turno}}">{{$item->turno}}</option>
                  <option value="Mañana">Mañana</option>
                  <option value="Tarde">Tarde</option>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Descripción</label>
              <div class="controls">
                <textarea name="descripcion" class="span7" >{{$item->descripcion}}</textarea>
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Aceptar</button>
            </div>
          {!!Form::close()!!}
          </div>
        </div>
</div>

@endsection
