<!DOCTYPE html>
<html lang="en">
<head>
<title>Sistema Inscripción</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />    
<meta id="token" name="csrf-token" content="{{ csrf_token() }}" value="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/bootstrap-responsive.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/fullcalendar.css') }}" />
<link rel="stylesheet" href="{{ asset('css/matrix-style.css') }}" />
<link rel="stylesheet" href="{{ asset('css/matrix-media.css') }}" />
<link href="{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{ asset('css/jquery.gritter.css') }}" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

<!--Header-part-->
<div id="header">
 <img src="{{asset('img/logo.jpg')}}" width="170px" height="200px"  alt="">
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li   ><a title="" href="{{url('/home')}}" ><span class="text">Bienvenido {{Auth()->user()->name}}</span></a>
       <li   ><a title="" href="{{url('/home')}}" ><span class="text"> Inicio </span></a>
      
    </li>
    <li class=""> <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
  </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->

<!--close-top-serch-->
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Panel de Administración</a>
  <ul>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Registrar</span></a>
      <ul>
        <li > <a href="{{url('alumnos/create')}}"> Inscripción</a> </li>
        <li > <a href="#myModal" data-toggle="modal">  Representante</a> </li>
        <li > <a href="{{url('profesores/create')}}">  Profesor</a> </li>
        <li > <a href="{{url('notas/create')}}"> Notas</a> </li>
        @if(Auth::user()->nivel == 0)
        <li > <a href="{{url('cuentas/create')}}"> Cuentas</a> </li>
        @endif
      </ul>
    </li>

    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Consultar y modificar</span></a>
      <ul>
        <li > <a href="{{url('inscripcion')}}"> Inscripción</a> </li>
        <li > <a href="{{url('representantes')}}"> Representante</a> </li>
        <li > <a href="{{url('alumnos')}}"> Alumno</a> </li>
        <li > <a href="{{url('profesores')}}">  Profesor</a> </li>
        <li > <a href="{{url('notas/index')}}">  Notas</a> </li>
      </ul>
    </li>

    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Ver reportes</span></a>
      <ul>
          <li > <a href="{{url('alumnospdf')}}">  Alumno</a> </li>
          <li > <a href="{{url('inscripcionespdf')}}">  Inscripción</a> </li>
          <li > <a href="{{url('representantespdf')}}">Representante</a> </li>
          <li > <a href="{{url('profesorespdf')}}">  Profesor</a> </li>
      </ul>
    </li>
  </ul>
</div>
<!--sidebar-menu-->

<!--main-container-part-->
<div id="content">

  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li><h4>{{ $error }}</h4></li>
            @endforeach
        </ul>
    </div>
@endif
 @yield('content')
   <div class="widget-content"> 

            <div id="myModal" class="modal hide">
              <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">×</button>
                <h3>Alerta</h3>
              </div>
              <div class="modal-body">
                <form class="form-horizontal" method="POST" action="{{ route('validarcedula') }}">
                        {{ csrf_field() }}
                <label for="representante"> Ingrese la cedula del representante, sin puntos.</label>
                 <input type="text" name="cedula" maxlength="8">
                 <button type="submit">Comprobar</button>
               </form>
              </div>
            </div>
            
          </div>
<div class="row-fluid">
  <div id="footer" class="span12"> 2018 &copy; Unidad Educativa Simoncito "Las Americas" <a href="http://themedesigner.in"> - Desarrollado por: Francisco La Rosa</a> </div>
</div>

<!--end-Footer-part-->

<script src="{{ asset('js/excanvas.min.js') }}"></script> 
<script src="{{ asset('js/jquery.min.js') }}"></script> 
<script src="{{ asset('js/jquery.ui.custom.js') }}"></script> 
<script src="{{ asset('js/bootstrap.min.js') }}"></script> 
<script src="{{ asset('js/jquery.flot.min.js') }}"></script> 
<script src="{{ asset('js/jquery.flot.resize.min.js') }}"></script> 
<script src="{{ asset('js/jquery.peity.min.js') }}"></script> 
<script src="{{ asset('js/fullcalendar.min.js') }}"></script> 
<script src="{{ asset('js/matrix.js') }}"></script> 
<script src="{{ asset('js/matrix.dashboard.js') }}"></script> 
<script src="{{ asset('js/jquery.gritter.min.js') }}"></script> 
<script src="{{ asset('js/matrix.interface.js') }}"></script> 
<script src="{{ asset('js/matrix.chat.js') }}"></script> 
<script src="{{ asset('js/jquery.validate.js') }}"></script> 
<script src="{{ asset('js/matrix.form_validation.js') }}"></script> 
<script src="{{ asset('js/jquery.wizard.js') }}"></script> 
<script src="{{ asset('js/jquery.uniform.js') }}"></script> 
<script src="{{ asset('js/select2.min.js') }}"></script> 
<script src="{{ asset('js/matrix.popover.js') }}"></script> 
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script> 
<script src="{{ asset('js/matrix.tables.js') }}"></script> 
<script src="{{ asset('vue/vue.js')}}"></script>
<script src="{{ asset('vue/vue-resource.min.js')}}"></script>
<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
@yield('js')
</body>
</html>
       
    