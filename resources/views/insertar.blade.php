@extends('layouts.app')

@section('content')

 <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Insertar</a></div>
  </div>

  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li><h4>{{ $error }}</h4></li>
            @endforeach
        </ul>
    </div>
@endif

<div  class="quick-actions_homepage">
    <ul class="quick-actions">
      <li class="bg_lo"> <a href="{{url('alumnos/create')}}"> <i class="icon-group"></i> Inscripción</a> </li>
      <li class="bg_lo"> <a href="#myModal" data-toggle="modal"> <i class="icon-user"></i> Representante</a> </li>
      <li class="bg_lo"> <a href="{{url('profesores/create')}}"> <i class="icon-book"></i> Profesor</a> </li>
      <li class="bg_lo"> <a href="{{url('notas/create')}}"> <i class="icon-folder-open"></i> Notas</a> </li>
      <li class="bg_lo"> <a href="{{url('cuentas/create')}}"> <i class="icon-ok"></i> Cuentas</a> </li>
    </ul>
  </div>

   <div class="widget-content"> 

            <div id="myModal" class="modal hide">
              <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">×</button>
                <h3>Alerta</h3>
              </div>
              <div class="modal-body">
                <form class="form-horizontal" method="POST" action="{{ route('validarcedula') }}">
                        {{ csrf_field() }}
                <label for="representante"> Ingrese la cedula del representante, sin puntos.</label>
                 <input type="text" name="cedula" maxlength="8">
                 <button type="submit">Comprobar en el sistema</button>
               </form>
              </div>
            </div>
            
          </div>
@endsection

