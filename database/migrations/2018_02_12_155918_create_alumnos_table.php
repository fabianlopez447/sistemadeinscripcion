<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identificador');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('fecha_nac');
            $table->string('fecha_ing');
            $table->string('partida_nac');
            $table->string('aula');
            $table->string('seccion');
            $table->string('turno');
            $table->string('cedula_rep');
            $table->string('descripcion');
            $table->integer('id_profesor');
            $table->integer('id_representante')->unsigned();
            $table->foreign('id_representante')->references('id')->on('representantes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
