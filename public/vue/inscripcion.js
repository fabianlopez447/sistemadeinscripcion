Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

 var app = new Vue({
      el: '#app',
      data: {
        mostrar: false,
        cedula: '',
        representante: [],
        mostrarcampos: false,
        nuevo: false,
        cantidad: null
      },
      methods: {
       validar: function(){
       	var input = this.cedula;
       		this.$http.get('/alumno/validar/'+this.cedula).then((response) => {
       			this.representante = response.data;
            this.mostrarcampos = true;
            this.nuevo = false;

            this.$http.get('/alumno/getalumno/'+this.representante.id).then((response) => {
              this.cantidad = response.data.total
             });

			     }, response=>{
              this.mostrarcampos = false;
              this.nuevo = true;
           });
        }
       }
    });
