<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Representante;
use \App\User;
use \App\Alumno;
use \App\Profesor;

class profesoresController extends Controller
{
    public function index()
    {
        $items = Profesor::orderBy('id', 'dsc')->paginate(200);
        return view('consultar.profesor.index', compact('items'));
    }
	 public function create( ){
        return view('insertar.profesor.create');
    }
    public function store( Request $request){
        Profesor::create($request->all());
        return redirect('/home');
    }
    public function edit($id)
    {
    	$item = Profesor::find($id);
    	return view('consultar.profesor.edit', compact('item'));
    }
    public function update($id, Request $request)
    {
    	$item = Profesor::find($id);
    	$item->fill($request->all());
    	$item->save();
    	return redirect('/profesores');
    }
     public function cedula(Request $request)
    {
        $request->validate([
            'cedula' => 'required|unique:profesor|min:7|max:8',
        ]);
        return response()->json('success');
    }
}
