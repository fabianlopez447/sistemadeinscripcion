<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nota;
use App\Alumno;
use App\Profesor;

class notasController extends Controller
{
    public function index(){
        $items = Alumno::orderBy('id','asc')->paginate(200);
        return view('consultar.notas.index',compact('items'));
    }

    public function create(){
       $alumns = Alumno::orderBy('id', 'asc')->get();
       $profs = Profesor::orderBy('cedula', 'asc')->get();
       return view('insertar.notas.create', compact('alumns','profs'));
    }

    public function store(Request $request){
        Nota::create($request->all());
        return redirect('/home');
    }

    public function edit($id)
    {
        $item = Nota::find($id);
        return view('consultar.notas.edit', compact('item'));
    }
    public function update($id, Request $request)
    {
        $item = Nota::find($id);
        $item->fill($request->all());
        $item->save();
        return redirect('/notas/index');
    }
    public function consultar($id, Request $request){
        $items = Nota::where('id_alumno', $id)->orderBy('id','asc')->paginate(200);
        return view('consultar.notas.ver',compact('items'));
    }
    /*public function edit($id){
    	$item = Nota::find($id);
    }
    public function update($id, Request $request){
    	$item = Nota::find($id);
    	$item->fill($request->all());
    	$item->save();
    }
    public function destroy($id)
    {
    	$item = Nota::find($id);
    	$item->delete(); 
        return back();  	
    }
    public function show(){
        return "PUTO";
    }*/
}
