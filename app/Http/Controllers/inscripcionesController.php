<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Representante;
use \App\User;
use \App\Alumno;
use \App\Profesor;

class inscripcionesController extends Controller
{
	public function index()
    {
    	$items = Alumno::orderBy('id','asc')->paginate(200);
    	return view('consultar.inscripcion.index', compact('items'));
    }
     public function edit($id){
    	$item = Alumno::find($id);
        $repres = Representante::orderBy('cedula', 'asc')->get();
        $prof= Profesor::orderBy('cedula','id')->get();
        return view('consultar.inscripcion.edit', compact('item','repres','prof'));
    }
      public function update($id, Request $request){
    	$item = Alumno::find($id);
    	$item->fill($request->all());
    	$item->save();
        return redirect('/inscripcion');
    }
   
}
