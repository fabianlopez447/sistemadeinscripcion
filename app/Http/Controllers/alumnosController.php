<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;
use App\Representante;
use App\Nota;
use App\Profesor;
use Barryvdh\DomPDF\Facade as PDF;

class alumnosController extends Controller
{
	public function index(){
		$items = Alumno::orderBy('id','asc')->paginate(200);
		return view('consultar.alumno.index',compact('items'));
	}
    public function create( ){
    	$repres = Representante::orderBy('cedula', 'asc')->get();
    	$prof = Profesor::orderBy('cedula', 'asc')->get();
        return view('insertar.alumnos.create',compact('repres','prof'));
    }
    public function store( Request $request){
        Alumno::create($request->all());
        return redirect('/home');
    }
    public function validar($cedula)
    {
        $item = Representante::where('cedula', $cedula)->first();
        if ($item) {
           return response()->json($item,200);
        }else{
            return response()->json($item,500);
        }

    }
		public function getAlumno($id){
			$item = Alumno::where('id_representante', $id)->paginate(20);
			return response()->json($item);
		}
     public function edit($id){
    	$item = Alumno::find($id);
        $repres = Representante::orderBy('cedula', 'asc')->get();
        $prof= Profesor::orderBy('cedula','id')->get();
        return view('consultar.alumno.edit', compact('item','repres','prof'));
    }
      public function update($id, Request $request){
    	$item = Alumno::find($id);
    	$item->fill($request->all());
    	$item->save();
        return redirect('/alumno');
    }
    public function reporte($id){
        $item = Alumno::find($id);
        $pdf = PDF::loadView('reportepdf.inscalumnopdf',compact('item'));
        return $pdf->stream('alumnos.pdf');
    }
}
