<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Representante;
use \App\User;
use \App\Alumno;

class representantesController extends Controller
{
	public function index()
    {
        $items = Representante::orderBy('cedula', 'asc')->paginate(200);
    	return view('consultar.representante.index', compact('items'));
    }
    public function validar(Request $request){
        $validatedData = $request->validate([
        'cedula' => 'required|unique:representantes|min:1000000|max:99999999|numeric',
         ]);
        return redirect()->route('representante.create', $request->cedula);
    }
    public function create($cedula){
        return view('insertar.representante.create', compact('cedula'));
    }
    public function store(Request $request){
    	Representante::create($request->all());
        $item = Representante::orderBy('created_at', 'desc')->first();
       return redirect('/home');
    }
    public function edit($id){
        $item = Representante::find($id);
        return view('consultar.representante.edit', compact('item'));
    }
    public function update($id, Request $request)
    {
        $item = Representante::find($id);
        $item->fill($request->all());
        $item->save();
        return redirect('/representantes');
    }
  /*  public function edit($id){
    	$item = Representante::find($id);
        return view('admin.representantes.edit', compact('item'));
    
    public function update($id, Request $request){
    	$item = Representante::find($id);
    	$item->fill($request->all());
    	$item->save();
        return redirect('/representante');
    }
    public function show(){
        return "Error de metodo de envio de datos";
    }
    public function destroy($id)
    {
    	$item = Representante::find($id);
    	$item->delete();   
        return back();	
    }
    public function alumnoCreate($id) {
        $item = Representante::find($id);
        return view('admin.representantes.alumnos.create', compact('item'));
    }
    public function verAlumnos($id){
        $items = Alumno::where('id_representante', $id)->orderBy('cedula', 'asc')->paginate(15);
        return view('admin.alumnos.index', compact('items'));
    }*/
}
