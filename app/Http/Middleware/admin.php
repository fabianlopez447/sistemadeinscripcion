<?php

namespace App\Http\Middleware;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Closure;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->nivel == 0) {
            return $next($request);
        }
        return redirect('/home');
    }
}
