<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alumno extends Model
{
    protected $table = 'alumnos';
    protected $fillable = [
    	'nombres',
    	'apellidos',
    	'fecha_nac',
    	'fecha_ing',
    	'partida_nac',
    	'cedula_rep',
    	'id_representante',
    	'id_profesor',
        'aula',
        'turno',
        'seccion',
        'descripcion'
    ];
    public function representante()
    {
    	return $this->belongsTo('App\Representante', 'id_representante', 'id');
    }
     public function profesor()
    {
    	return $this->belongsTo('App\Profesor', 'id_profesor', 'id');
    }
   
}
