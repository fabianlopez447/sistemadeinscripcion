<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
     protected $table = 'notas';
    protected $fillable = [
    	'materia',
    	'calificacion',
    	'id_alumno',
    	'id_profesor'
    ];
    public function alumno()
    {
    	return $this->belongsTo('App\Alumno', 'id_alumno', 'id');
    }
}
