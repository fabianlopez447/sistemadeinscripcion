<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representante extends Model
{
    protected $table = 'representantes';
    protected $fillable = [
    	'nombre',
    	'apellidos',
    	'cedula',
    	'telefono',
    	'correo',
    	'direccion',
    ];
   /* public function alumno()
    {
    	return $this->hasMany('App\Alumno');
    }*/

}